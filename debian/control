Source: wmmon
Section: x11
Priority: optional
Maintainer: Debian Window Maker Team <pkg-wmaker-devel@lists.alioth.debian.org>
Uploaders: Doug Torrance <dtorrance@piedmont.edu>
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 10),
               libdockapp-dev,
               libx11-dev,
               libxext-dev,
               libxpm-dev,
               pkg-config
Homepage: http://www.dockapps.net/wmmon
Vcs-Browser: https://salsa.debian.org/wmaker-team/wmmon
Vcs-Git: https://salsa.debian.org/wmaker-team/wmmon.git

Package: wmmon
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Window Maker dockapp for monitoring system information
 WMMon monitors the realtime CPU load as well as the average system load,
 and gives you some nice additional features too. It is intended for
 docking in Window Maker.
 .
 It currently provides:
 .
   * a realtime CPU stress meter;
   * an auto-scaled average system load meter, like xload and wmavgload;
   * a realtime disk I/O stress meter;
   * auto-scaled disk I/O load meter;
   * realtime memory and swap usage meters;
   * a display for system uptime;
   * three user-defined commands to launch.
